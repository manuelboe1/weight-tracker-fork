# Weight Tracker Fork
Weight Tracker is is an app that allows you to track your weight, including BMI Status and more statistics. This is a Fork from https://gitlab.com/wgarcia/weight-tracker

## License


This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.



## How to build the App

first, you have to install clickable and git for your Linux Distro

```
git clone https://gitlab.com/manuelboe1/weight-tracker-fork.git
cd weight-tracker-fork
clickable build
```

***
